$(document).ready(function() {
    var $list = $('#list');

    /**
     * Adding new element by key "Enter"
     */
    $('#input-text').keyup(function(eventObject) {
        if (eventObject.which == 13) $('#add').click();
    });

    /**
     * Adding new element by press button "Add"
     */
    $('#add').click(function() {
        var $input = $('#input-text');
        var insertText = $.trim($input.val());

        if (insertText) {
            var newLi =
                '<li>' +
                '   <input type="checkbox" class="check"/>' +
                '   <span>' +
                        insertText +
                '   </span>' +
                '   <input type="button" value="Edit" class="edit"/>' +
                '   <input type="button" value="Delete" class="delete"/>' +
                '</li>';
            $list.prepend(newLi);
            $input.val(null);
        }
    });

    /**
     * If checked checkbox - text strikethrough
     */
    $list.on('change', '.check', function() {
        var $span = $(this).next();

        if ($span.hasClass('marked')) {
            $span.removeClass('marked');
        } else {
            $span.addClass('marked');
        }
    });

    /**
     * Editing element by press button "Edit"
     */
    $list.on('click', '.edit', function() {
        var $span = $(this).prev();
        var result = prompt('Edit this value:', $span.text());

        if (!result) return;
        result = $.trim(result);
        if (typeof result == 'string' && result != '') {
            $span.text(result);
        } else {
            alert('Type text!');
        }
    });

    /**
     * Deleting element by press button "Edit"
     */
    $list.on('click', '.delete', function() {
        $(this).parent().remove();
    });

    /**
     * Hide/Show strikethrough elements by press button "Hide" or "Show"
     */
    $('#hideShow').click(function() {
        if ($(this).val() == 'Hide') {
            $('.marked').each(function(i, elem) {
                $(elem).parent().hide();
            });
            $(this).val('Show');
        } else {
            $('.marked').each(function(i, elem) {
                $(elem).parent().show();
            });
            $(this).val('Hide');
        }
    });
});